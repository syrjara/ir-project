import ir_datasets
from flask import Flask, request, jsonify
# from services.TextProccessingService import TextProcessing
# from services.indexing import IndexingService
from services.CosineSimilaritySearch import CosinSimilaritySearch
from services.queryStore import refinement, storeQueries, add_refinement
from flask_cors import CORS

app = Flask(__name__)
CORS(app, supports_credentials=True)
# import pickle
import time
from services.QueryProcessing import QueryService
start_time = time.time()
# dataset1 = ir_datasets.load("lotte/writing/test/forum")
# dataset2 = ir_datasets.load("antique/test")
# #######################################
# correct_dataset1 = TextProcessing.text_processing(dataset1)
# with open("model/correct_dataset1.pkl", "wb") as f:
#     pickle.dump(correct_dataset1, f)
# with open("model/correct_dataset1.pkl", "rb") as f:
#     loaded_dataset1 = pickle.load(f)
# IndexingService.Vectorizer(loaded_dataset1, 1)
# ChromaService.ChromaIndexer(1)
# storeQueries(dataset1, 1)
############################################################################

# correct_dataset2 = TextProcessing.text_processing(dataset2)
# with open("model/correct_dataset2.pkl", "wb") as f:
#     pickle.dump(correct_dataset2, f)
# with open("model/correct_dataset2.pkl", "rb") as f:
#     loaded_dataset2 = pickle.load(f)
# IndexingService.Vectorizer(loaded_dataset2, 2)
# ChromaService.ChromaIndexer(2)
# storeQueries(dataset2, 2)


#######################################
@app.route("/search", methods=["POST"])
def search():
    data = request.json
    query = data.get("query")
    dataset = data.get("dataset")
    if query or dataset:
        query_df = QueryService.queryProccessing(query, dataset)
        if query_df.empty:
            return jsonify({"error": "Query processing failed"}), 500
        results = CosinSimilaritySearch(query_df, dataset)
        results_list = results.to_dict(orient="records")
        add_refinement(dataset, query)
        return jsonify(results_list), 200


#################################################################
@app.route("/query-refinement", methods=["POST"])
def query_refinement():
    data = request.json
    query = data.get("query")
    dataset = data.get("dataset")
    if query or dataset:
        results = refinement(query, dataset)
        return jsonify(results), 200


if __name__ == "__main__":
    app.run(debug=True)
print("Time taken: %s seconds " % (time.time() - start_time))


#######################################
# Test Case:
# from services.Chroma import ChromaService
# query_df1 = QueryService.queryProccessing("look", 1)
# ChromaService.ChromaSearch(query_df1, 1)
