import mysql.connector
import ir_datasets
import pickle
import re

dataset1 = ir_datasets.load("lotte/writing/test/forum")
dataset2 = ir_datasets.load("antique/test")


@staticmethod
def storeQueries(document, number):
    documents_list = []
    for docs in document.queries_iter():
        doc_dict = {
            "text": docs.text,
        }
        documents_list.append(doc_dict)
    if number == 1:
        with open("model/queries1.pkl", "wb") as f:
            pickle.dump(documents_list, f)
    elif number == 2:
        with open("model/queries2.pkl", "wb") as f:
            pickle.dump(documents_list, f)
    print("Queries are stored..")


def refinement(query, number):
    if number == 1:
        with open("model/queries1.pkl", "rb") as f:
            queries = pickle.load(f)
    elif number == 2:
        with open("model/queries2.pkl", "rb") as f:
            queries = pickle.load(f)
    print(queries)
    # Use regex to search for the query substring in the query text
    pattern = re.compile(query, re.IGNORECASE | re.DOTALL)
    results = []
    for q in queries:
        if len(results) == 10:
            break
        if pattern.search(q["text"]):
            results.append(q)
    print(results)
    return results  # Return the matching query dictionary


def add_refinement(dataset, query):
    if dataset == 1:
        file_path = "model/queries1.pkl"
    elif dataset == 2:
        file_path = "model/queries2.pkl"

    try:
        with open(file_path, "rb") as f:
            queries = pickle.load(f)
    except FileNotFoundError:
        queries = []

    # Check if the query already exists
    if not any(q["text"] == query for q in queries):
        queries.append({"text": query})

        with open(file_path, "wb") as f:
            pickle.dump(queries, f)
        print("Query added.")
