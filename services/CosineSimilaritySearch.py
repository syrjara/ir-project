import pickle
from sklearn.metrics.pairwise import cosine_similarity
import pandas as pd
import mysql.connector


def CosinSimilaritySearch(query_df, number):
    if number == 1:
        with open("model/vectors1.pkl", "rb") as file:
            vectors, doc_ids = pickle.load(file)
    elif number == 2:
        with open("model/vectors2.pkl", "rb") as file:
            vectors, doc_ids = pickle.load(file)
    cosine_similarities = cosine_similarity(query_df, vectors).flatten()
    similarity_df = pd.DataFrame({"doc_id": doc_ids, "similarity": cosine_similarities})
    sorted_similarity_df = similarity_df.sort_values(by="similarity", ascending=False)
    top_10_results = sorted_similarity_df.head(10)
    if number == 1:
        db_name = "dataset1"
    elif number == 2:
        db_name = "dataset2"
    db_config = {
        "user": "root",
        "password": "",
        "host": "127.0.0.1",
        "database": db_name,
    }
    db_connection = mysql.connector.connect(**db_config)
    cursor = db_connection.cursor(dictionary=True)
    top_10_results["text"] = top_10_results["doc_id"].apply(
        lambda doc_id: fetch_text(cursor, doc_id)
    )
    db_connection.close()
    return top_10_results


def fetch_text(cursor, doc_id):
    query = "SELECT text FROM documents WHERE doc_id = %s"
    cursor.execute(query, (doc_id,))
    result = cursor.fetchone()
    return result["text"] if result else ""
