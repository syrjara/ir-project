from typing import List
from spellchecker import SpellChecker
from nltk import word_tokenize


class SpellCheckingService:

    def correct_sentence_spelling(tokens: List[str]) -> List[str]:
        spell = SpellChecker()
        misspelled = spell.unknown(tokens)
        for i, token in enumerate(tokens):
            if token in misspelled:
                corrected = spell.correction(token)
                if corrected is not None:
                    tokens[i] = corrected
        return tokens

    def spell_check(lemmatized_documents_list):
        spellChecked_documents_list = []
        count = 0
        for lemmatized_document in lemmatized_documents_list:
            words = word_tokenize(lemmatized_document["text"])
            corrected_text = SpellCheckingService.correct_sentence_spelling(words)
            correct = " ".join(corrected_text)
            correct_documentt = {
                "doc_id": lemmatized_document["doc_id"],
                "text": correct,
            }
            spellChecked_documents_list.append(correct_documentt)
            count += 1
            print("spellChecked_documents:" + str(count))
        print("spellChecked_documents Successfully..")
        return spellChecked_documents_list
