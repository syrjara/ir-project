import pickle
import pandas as pd
from services.TextProccessingService import TextProcessing


class QueryService:
    def queryProccessing(query, dataset):
        if dataset == 1:
            with open("model/my_tfidf_vectorizer1.pkl", "rb") as f:
                loaded_vectorizer = pickle.load(f)
        elif dataset == 2:
            with open("model/my_tfidf_vectorizer2.pkl", "rb") as f:
                loaded_vectorizer = pickle.load(f)
        processed_query = TextProcessing.query_processing(query)
        query_vector = loaded_vectorizer.transform([processed_query])
        query_df = pd.DataFrame(
            query_vector.toarray(), columns=loaded_vectorizer.get_feature_names_out()
        )
        return query_df
