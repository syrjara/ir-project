from nltk.tokenize import word_tokenize
from nltk.stem import SnowballStemmer
import nltk

stemmer = SnowballStemmer("english")

class StemmingService:
    @staticmethod
    def stem_document(normalized_documents_list):
        stemmer = SnowballStemmer("english")
        stemmed_documents_list = []
        count=0
        for normalized_document in normalized_documents_list:
            words = word_tokenize(normalized_document["text"])
            stemmed_words = [stemmer.stem(word) for word in words]
            stemmed_document = " ".join(stemmed_words)
            stemmed_documentt = {
                "doc_id": normalized_document["doc_id"],
                "text": stemmed_document,
            }
            stemmed_documents_list.append(stemmed_documentt)
            count+=1
            print('Stemming:' + str(count))
        print("stemmed_documents Successfully..")
        return stemmed_documents_list
