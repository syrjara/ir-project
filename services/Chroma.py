import chromadb
import pickle
# import mysql.connector
chroma_client = chromadb.PersistentClient(
    path="C:/Users/XPRISTO/Desktop/ir-project/chroma"
)


class ChromaService:
    @staticmethod
    def ChromaIndexer(number):
        collection_name = f"my_vectors{number}"
        try:
            chroma_client.delete_collection(name=collection_name)
        except:
            pass
        collection = chroma_client.create_collection(
            name=collection_name, metadata={"hnsw:space": "cosine"}
        )
        with open(f"model/vectors_chroma{number}.pkl", "rb") as f:
            df = pickle.load(f)
        chunk_size = 100
        num_docs = len(df)
        for start in range(0, num_docs, chunk_size):
            end = min(start + chunk_size, num_docs)
            chunk_df = df.iloc[start:end]
            ids = [str(doc_id) for doc_id in chunk_df.index]
            vectors = chunk_df.values.tolist()
            metadatas = [{"doc_id": str(doc_id)} for doc_id in chunk_df.index]

            collection.upsert(ids=ids, embeddings=vectors, metadatas=metadatas)
            print(f"Processed {end} documents")

        print("Storing successfully...")

    def ChromaSearch(query_df, number):
        if number == 1:
            collection = chroma_client.get_collection(name="my_vectors1")
        elif number == 2:
            collection = chroma_client.get_collection(name="my_vectors2")
        query_vector_list = query_df.iloc[0].tolist()
        query_results = collection.query(query_embeddings=[query_vector_list])
        id_distance_pairs = zip(query_results["ids"][0], query_results["distances"][0])
        sorted_results = sorted(id_distance_pairs, key=lambda x: x[1])
        top_results = sorted_results[:10]
        result_list = [{"id": id, "distance": distance} for id, distance in top_results]
        print(result_list )