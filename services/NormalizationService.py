from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords, words
import string
import re


class NormalizationService:
    @staticmethod
    def normalize_document(documents_list):
        normalized_documents_list = []
        count = 0
        english_words = set(words.words("en"))
        stop_words = set(stopwords.words("english"))

        for document in documents_list:
            text = document["text"]
            
            # Remove punctuation and replace with space
            text = text.translate(str.maketrans(string.punctuation, ' ' * len(string.punctuation)))

            # Tokenize the text
            words_in_text = word_tokenize(text)
            
            # Convert to lowercase
            words_in_text = [word.lower() for word in words_in_text]
            
            # Remove stopwords
            words_in_text = [word for word in words_in_text if word not in stop_words]
            
            # Filter out non-alphabetic and non-English words
            words_in_text = [
                word for word in words_in_text
                if word.isalpha() and word in english_words
            ]
            normalized_document = " ".join(words_in_text)
            normalized_documentt = {
                "doc_id": document["doc_id"],
                "text": normalized_document,
            }
            count += 1
            normalized_documents_list.append(normalized_documentt)
            print(f"Normalizing: {count}")

        print("Normalized Successfully.")
        return normalized_documents_list
