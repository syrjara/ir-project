from services.LemmatizationService import LemmatizationService
from services.NormalizationService import NormalizationService
from services.StemmingService import StemmingService
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords, words
from nltk.stem import SnowballStemmer, WordNetLemmatizer, PorterStemmer
from nltk.corpus import wordnet
from nltk import pos_tag
import string

# from services.SpellCheckingService import SpellCheckingService


class TextProcessing:
    def text_processing(document):
        documents_list = []
        count = 0
        for docs in document.docs_iter():
            # if count==100:
            #     break
            doc_dict = {"doc_id": docs.doc_id, "text": docs.text}
            count += 1
            print("files ready to proccessing : " + str(count))
            documents_list.append(doc_dict)
        normalized_document = NormalizationService.normalize_document(documents_list)
        stemmed_document = StemmingService.stem_document(normalized_document)
        lemmatized_document = LemmatizationService.lemmatize_document(stemmed_document)
        # corrected_document = SpellCheckingService.spell_check(lemmatized_document)
        return lemmatized_document

    @staticmethod
    def get_wordnet_pos(tag):
        tag_dict = {
            "J": wordnet.ADJ,
            "N": wordnet.NOUN,
            "V": wordnet.VERB,
            "R": wordnet.ADV,
        }
        return tag_dict.get(tag[0].upper(), wordnet.NOUN)

    @staticmethod
    def query_processing(query):
        stop_words = set(stopwords.words("english"))
        english_words = set(words.words("en"))
        stemmer = PorterStemmer()
        lemmatizer = WordNetLemmatizer()

        # Tokenize the text
        words_in_text = word_tokenize(query)

        # Remove punctuation and convert to lowercase
        words_in_text = [
            word.translate(str.maketrans("", "", string.punctuation)).lower()
            for word in words_in_text
        ]

        # Remove stopwords and non-alphabetic words, and ensure they are English words
        words_in_text = [
            word
            for word in words_in_text
            if word not in stop_words and word.isalpha() and word in english_words
        ]

        # Stem the words
        stemmed_words = [stemmer.stem(word) for word in words_in_text]

        # Get POS tags for the stemmed words
        pos_tags = pos_tag(stemmed_words)

        # Lemmatize the stemmed words with their POS tags
        lemmatized_words = [
            lemmatizer.lemmatize(word, pos=TextProcessing.get_wordnet_pos(tag))
            for word, tag in pos_tags
        ]

        # Join the lemmatized words into a single string
        lemmatized_query = " ".join(lemmatized_words)
        return lemmatized_query
