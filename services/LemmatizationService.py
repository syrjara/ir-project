from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from nltk.corpus import wordnet
from nltk import pos_tag

lemmatizer = WordNetLemmatizer()


class LemmatizationService:
  def get_wordnet_pos(tag_parameter):
      tag = tag_parameter[0].upper()
      tag_dict = {
          "J": wordnet.ADJ,
          "N": wordnet.NOUN,
          "V": wordnet.VERB,
          "R": wordnet.ADV,
      }
      return tag_dict.get(tag, wordnet.NOUN)

  def lemmatize_document(stemmed_documents_list):
        lemmatizer = WordNetLemmatizer()
        lemmatized_documents_list = []
        count = 0
        for stemmed_document in stemmed_documents_list:
            words = word_tokenize(stemmed_document["text"])
            pos_tags = pos_tag(words)
            lemmatized_words = [
                lemmatizer.lemmatize(word, pos=LemmatizationService.get_wordnet_pos(tag))
                for word, tag in pos_tags
            ]
            lemmatized_document = " ".join(lemmatized_words)
            lemmatized_documentt = {
                "doc_id": stemmed_document["doc_id"],
                "text": lemmatized_document,
            }
            lemmatized_documents_list.append(lemmatized_documentt)
            count += 1
            print('Lemmatization:' + str(count))
        print("lemmatized_documentt Successfully..")
        return lemmatized_documents_list
