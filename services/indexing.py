import pandas as pd
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
import chromadb
import time
import gc

# from scipy.sparse import csr_matrix, save_npz, load_npz

chroma_client = chromadb.PersistentClient(
    path="C:/Users/XPRISTO/Desktop/ir-project/chroma"
)


class IndexingService:
    @staticmethod
    def Vectorizer(spellChecked_documents_list, number):
        documents = []
        doc_ids = []
        count = 0
        for doc in spellChecked_documents_list:
            documents.append(doc["text"])
            doc_ids.append(doc["doc_id"])
            count += 1
            print(f"{str(count)} documents indexed.")
        vectorizer = TfidfVectorizer(min_df=3, max_df=0.85)
        tfidf_matrix = vectorizer.fit_transform(documents)
        df = pd.DataFrame(
            tfidf_matrix[:10000].toarray(),
            columns=vectorizer.get_feature_names_out(),
            index=doc_ids[:10000],
        )
        indexed_terms_count = len(vectorizer.vocabulary_)  # terms count
        print(f"Number of indexed terms: {indexed_terms_count}")
        if number == 1:
            with open("model/my_tfidf_vectorizer1.pkl", "wb") as f:
                pickle.dump(vectorizer, f)
            with open("model/vectors1.pkl", "wb") as f:
                pickle.dump((tfidf_matrix, doc_ids), f)
            with open("model/vectors_chroma1.pkl", "wb") as f:
                pickle.dump(df, f)
        elif number == 2:
            with open("model/my_tfidf_vectorizer2.pkl", "wb") as f:
                pickle.dump(vectorizer, f)
            with open("model/vectors2.pkl", "wb") as f:
                pickle.dump((tfidf_matrix, doc_ids), f)
            with open("model/vectors_chroma2.pkl", "wb") as f:
                pickle.dump(df, f)
        print("Indexing successful...")
