import mysql.connector
import ir_datasets

dataset1 = ir_datasets.load("lotte/writing/test/forum")
dataset2 = ir_datasets.load("antique/test")
def store(document, number):
    documents_list = []
    for docs in document.docs_iter():
        if number == 1:
            doc_dict = {
                "doc_id": docs.doc_id,
                "text": docs.text,
            }
        elif number == 2:
            doc_dict = {
                "doc_id": docs.doc_id,
                "text": docs.text,
            }
        documents_list.append(doc_dict)
    if number == 1:
        db_config = {
            "user": "root",
            "password": "",
            "host": "127.0.0.1",
            "database": "dataset1",
        }
        db_connection = mysql.connector.connect(**db_config)
        cursor = db_connection.cursor()
        create_table_query = """
        CREATE TABLE IF NOT EXISTS documents (
            doc_id VARCHAR(255) PRIMARY KEY,
            text TEXT
        )"""
        cursor.execute(create_table_query)
        insert_query = """
        INSERT INTO documents (doc_id, text)
        VALUES (%s, %s)
        """
        for doc in documents_list:
            cursor.execute(insert_query, (doc["doc_id"], doc["text"]))
        print("Store Data Successfully")
    elif number == 2:
        db_config = {
            "user": "root",  # default XAMPP MySQL user
            "password": "",  # default XAMPP MySQL password (usually empty)
            "host": "127.0.0.1",
            "database": "dataset2",
        }
        db_connection = mysql.connector.connect(**db_config)
        cursor = db_connection.cursor()
        create_table_query = """
        CREATE TABLE IF NOT EXISTS documents (
            doc_id VARCHAR(255) PRIMARY KEY,
            text TEXT
        )"""
        cursor.execute(create_table_query)
        insert_query = """
        INSERT INTO documents (doc_id, text)
        VALUES (%s, %s)
        """
        for doc in documents_list:
            cursor.execute(insert_query, (doc["doc_id"], doc["text"]))
    print("Store Data Successfully")
    db_connection.commit()
    db_connection.close()


# store(dataset1, 1)
store(dataset2, 2)
print("Documents have been stored in the database.")
