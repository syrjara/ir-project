import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, map } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  base_url = 'http://localhost:5000';
  dataset !:number|null
  constructor(private http: HttpClient) {}
  search(data: any): Observable<any> {
    return this.http.post(this.base_url + '/search', data);
  }
  queryRefinement(value: any): Observable<any> {
    let payload:any;
    payload = {
      query: value,
      dataset:this.dataset
    };
    return this.http
      .post(this.base_url + '/query-refinement', payload)
      .pipe(map((res) => res));
  }
}
