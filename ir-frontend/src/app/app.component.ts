import { Component, OnInit, inject } from '@angular/core';
import {
  FormControl,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { AppService } from './app.service';
import { CommonModule } from '@angular/common';
import {
  Observable,
  debounceTime,
  distinctUntilChanged,
  of,
  startWith,
  switchMap,
} from 'rxjs';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatIconModule,
    MatTooltipModule,
    MatButtonToggleModule,
    MatAutocompleteModule,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  appService = inject(AppService);
  key = new FormControl('', Validators.required);
  selectedDataset = new FormControl(1, Validators.required);
  results: any[] = [];
  datasets: number[] = [1, 2];
  loading = false;
  filteredOptions!: Observable<any[]>;
  ngOnInit(): void {
    this.appService.dataset = this.selectedDataset.value;
    this.filteredOptions = this.key.valueChanges.pipe(
      debounceTime(300), // Add a debounce time to limit the number of API calls
      distinctUntilChanged(), // Only call the API if the value has changed
      switchMap((value) => {
        return this.appService.queryRefinement(value);
      })
    );
  }
  selectDataset(value:number){
    this.appService.dataset=value
  }

  search() {
    let payload: any;
    payload = {
      query: this.key.value,
      dataset: this.selectedDataset.value,
    };
    this.loading = true;
    this.key.disable();
    this.selectedDataset.disable()
    this.appService.search(payload).subscribe((res: any) => {
      this.results = res;
      this.loading = false;
      this.key.enable();
      this.selectedDataset.enable()
    });
  }
  searchOnSelect(event:any){
    this.key.setValue(event.source.value);
    this.search()
  }
}
